/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.softlogicproject.service;

import com.mycompany.softlogicproject.dao.ProductDao;
import com.mycompany.softlogicproject.model.Product;
import java.util.ArrayList;

/**
 *
 * @author User
 */
public class ProductService {
    private ProductDao productDao = new ProductDao();
    public ArrayList<Product> getProductsOrderByName() {
       return (ArrayList<Product>) productDao.getAll("product_name ASC");
    }
}
