/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.mycompany.softlogicproject.component;

import com.mycompany.softlogicproject.model.Product;

/**
 *
 * @author User
 */
public interface BuyProductable {
    public void buy(Product product, int qty);
}
